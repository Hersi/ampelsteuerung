\contentsline {section}{\numberline {1}Aufgabenstellung und Zielsetzung}{2}
\contentsline {section}{\numberline {2}Versuchsaufbau}{3}
\contentsline {section}{\numberline {3}Modellierung}{4}
\contentsline {subsection}{\numberline {3.1}Modellierung der Zyklischen Abfolge}{4}
\contentsline {subsection}{\numberline {3.2}Ermitteln der Zust\"andsbitfolgen durch die Schieberegister}{4}
\contentsline {subsection}{\numberline {3.3}Zustandswechsel mit Taster}{5}
\contentsline {section}{\numberline {4}Implementierung}{6}
\contentsline {subsection}{\numberline {4.1}Pin-Belegung der Komponenten}{6}
\contentsline {subsection}{\numberline {4.2}Die Deltatabelle f\"ur den Zustandswechsel}{6}
\contentsline {subsection}{\numberline {4.3}Die Initialisierung}{7}
\contentsline {subsection}{\numberline {4.4}Der zyklische Ablauf}{8}
\contentsline {subsection}{\numberline {4.5}Der Zustandswechsel nach einem Tastendruck}{10}
\contentsline {section}{\numberline {A}Quellcode}{11}
\contentsline {section}{\numberline {B}Schieberegister}{15}
