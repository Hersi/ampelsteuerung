//Pinbelegung f�r AmpelLED's
#define SER_PIN 34 
#define SCK_PIN 35
#define CLK_PIN 36  
#define RCK_PIN 37  

//Pinbelegung f�r Taster
#define SER2_PIN 7
#define PL_PIN 6
#define CLK2_PIN 5

//Initialisierung der Zust�nde als Bitfolge
//                                     Oben links, Oben rechts, unten rechts, unten links
const unsigned long VAblauf[8][4] = { {0b00110011,0b10001100,0b00110011,0b10001100}, //Z1
                                      {0b01010011,0b10001100,0b01010011,0b10001100}, //Z2
                                      {0b10010101,0b10010100,0b10010101,0b10010100},//Z3/Z7
                                      {0b10001101,0b11010010,0b10001101,0b11010010},//Z4 
                                      {0b10001101,0b00110010,0b10001101,0b00110010},//Z5 
                                      {0b10001101,0b01010010,0b10001101,0b01010010},//Z6 
                                      {0b10010101,0b10010100,0b10010101,0b10010100},//Z3/Z7 
                                      {0b11010011,0b10001100,0b11010011,0b10001100} //Z8 
                                    };


//DeltaTabelle Taster HS, QS: TasterHS, TasterQS, Z>Timeout, Z<Timeout
// - bedeutet, dass kein Zustandswechsel erfolgen darf
int zTabelle [8][4] = {   {-1, 1, 1, 0},
                          {-1, -1, 2, 1},
                          {6, -1, 3, 2},
                          {-1, -1, 4, 3},
                          {5, -1, 5, 4},
                          {-1, -1, 6, 5},
                          {-1, 2, 7, 6},
                          {-1, -1, 0, 7}};      
//                                                rot                    rot
//                               gr�n, gelb, rot, gelb, gr�n, gelb, rot, gelb  -> Verkehrsampeln   
const unsigned long VAzeit[8] = {5000, 2000, 5000, 2000, 5000, 2000, 5000, 2000};  

//Initialisierung des neuen Zustands nach Tastendruck 
int stateneu = -1;     

bool state_0 = false;
bool state_4 = false;


/*******************************
Setup der Pinbelegungen
********************************/
void setup()
{
  Serial.begin(19200);
  
  pinMode(SER_PIN, OUTPUT);
  pinMode(SCK_PIN, OUTPUT);
  pinMode(RCK_PIN, OUTPUT);
  pinMode(CLK_PIN, OUTPUT);
  pinMode(SER2_PIN, INPUT);
  pinMode(PL_PIN, OUTPUT);
  pinMode(CLK2_PIN, OUTPUT);
  
  digitalWrite(SER_PIN,LOW);
  digitalWrite(SCK_PIN,LOW);
  digitalWrite(RCK_PIN,LOW);
  digitalWrite(CLK_PIN,HIGH);
  
  digitalWrite(PL_PIN, LOW);
  digitalWrite(CLK2_PIN, LOW);
}

/*******************************
Funktion zum einlesen der Taster
@param int state (Aktueller Zustand)
@return int (R�ckgabe des zu wechselnden Zustands)
********************************/
int Taster(int state1){
  
    uint8_t u8_data[2];
    uint16_t i, temp = 0;
    
    //Einlesen von den 16-Bit Tasterzust�nde
    digitalWrite(PL_PIN, HIGH);
    //2 * 8 = 16 Bit werden eingelesen...
    for(i=0; i<2; i++){
        u8_data[i] = shiftIn(SER2_PIN, CLK2_PIN, MSBFIRST);
    }
    digitalWrite(PL_PIN, LOW);
    
    //Abfrage welche Taster gedr�ckt wurde
    if (u8_data[0] == 0b010 ||  u8_data[0] == 0b100000 ||
        u8_data[1] == 0b100000 || u8_data[1] == 0b10 ){    
          state_0 = true;
          return zTabelle[state1][0];
    }else if (u8_data[0] == 0b100  ||  u8_data[0] == 0b10000 ||
        u8_data[1] == 0b100 || u8_data[1] == 0b10000 ){
          state_4 = true;
          return zTabelle[state1][1];
    }  
  return -1;
}

/*******************************
Funktion f�r die Zeitsteuerung der Ampeln
********************************/
void Timer(void)
{
  static bool taster = true;
  static bool zwechsel = false;
  static unsigned int state = 0;
  static unsigned long time_last = millis();
  
  if(stateneu == -1 && taster){
    stateneu = Taster(state);
  }
    long milsek = millis();
    if(milsek - time_last <= VAzeit[state]){
      digitalWrite(RCK_PIN, LOW);
      //shiftOut sendet nur 8 Bits. Die schleife sendet 4 * 8 = 32 Bits
      
        //Wenn Tastendruck vorliegt
        if(stateneu > -1 && stateneu != state){
          if(state % 2 == 0){
            time_last -= 3000;
            //Taster sperren damit nicht in den �bern�chsten Zustand wechselt
            taster = false;
            //Falls Rotephase Tastendruck vorliegt.
            if(state == 2 || state == 6){
              state = stateneu;
            }
            //stateneu wird zur�ckgesetzt.
            stateneu = -1;
            return;
          }                     
        /*}else if(state_0 && zwechsel){
          if(state % 2 == 0){
            //Serial.print(state,DEC);
            //Serial.println(" Test ");
            //time_last -= 1000;
            //zwechsel = false;
            //Taster sperren damit nicht in den �bern�chsten Zustand wechselt
            //taster = false;
          } 
          //return;
        /*}else if (state_4 && zwechsel){
          if(state % 2 == 0){
            Serial.print(state);
            Serial.println(" Test2 ");
              //time_last -= 1000;
              zwechsel = false;
              //Taster sperren damit nicht in den �bern�chsten Zustand wechselt
              taster = false;
          }    
          return;*/
        }else{
          for (int j = 0; j < 4; j++){
            shiftOut(SER_PIN, SCK_PIN, LSBFIRST, VAblauf[state][j]);
          }
        }
        if(state_0 && zwechsel && taster){
          if(state % 2 == 0){
            //Serial.print(state,DEC);
            //Serial.println(" Test ");
            time_last -= 4000;
            zwechsel = false;
            //Taster sperren damit nicht in den �bern�chsten Zustand wechselt
            //taster = false;
          } 
        }
        
        if (state_4 && zwechsel && taster){
          if(state % 2 == 0){
            //Serial.print(state);
            //Serial.println(" Test2 ");
              time_last -= 4000;
              zwechsel = false;
              //Taster sperren damit nicht in den �bern�chsten Zustand wechselt
              //taster = false;
          }  
        } 

      digitalWrite(RCK_PIN, HIGH);
      return;
    }
    taster = true;
    //Wechselt den Zustand
    state = zTabelle[state][2];
    zwechsel = true;
    if(state_4 && state == 4)
      state_4 = false;
    if(state_0 && state == 0)
      state_0 = false;
     
    //Serial.println(state,DEC);
    time_last = millis();  
}

void loop(void)
{
 Timer();
}